provider "aws" {
  version                    = "2.41.0"
  region                     = "ap-southeast-1"
  skip_requesting_account_id = true
}


resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_security_group" "web-public" {
  name        = "web-public-sg"
  description = "Allow All inbound traffic 80/443"
  vpc_id      = aws_vpc.main.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "TLS from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

}

resource "aws_instance" "web-public" {
  ami           = "ami-0a2232786115639d7"
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.web-public.id]

}


############################################################

resource "aws_security_group" "web-application" {
  name        = "web-application-sg"
  description = "Allow web-public inbound traffic 8080"
  vpc_id      = aws_vpc.main.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = [aws_instance.web-public.public_ip,aws_vpc.main.cidr_block]
  }


  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

}

resource "aws_instance" "web-application" {
  ami           = "ami-0a2232786115639d7"
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.web-application.id]

}
